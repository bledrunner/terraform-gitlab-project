variable "name" {
}

variable "default_branch" {
  default = "master"
}

variable "visibility_level" {
  default = "private"
}