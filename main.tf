resource "gitlab_project" "main" {
  name             = var.name

  default_branch   = var.default_branch
  visibility_level = var.visibility_level
}